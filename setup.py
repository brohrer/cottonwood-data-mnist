import setuptools as st

st.setup(
    name='cottonwood_data_mnist',
    version='1',
    description='Mirror of MNIST in a Cottonwood block',
    url='https://gitlab.com/brohrer/cottonwood-data-mnist',
    download_url='https://gitlab.com/brohrer/cottonwood-data-mnist/-/archive/main/cottonwood-data-mnist-main.zip',
    author='Brandon Rohrer',
    author_email='brohrer@gmail.com',
    license='MIT',
    install_requires=["numpy"],
    package_data={
        "": [
            "mnist.npz",
            "mnist_sample.npz",
        ],
    },
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
