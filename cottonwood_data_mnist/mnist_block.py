from importlib import resources
import numpy as np


class TrainingData:
    def __init__(self, data_filename='mnist.npz'):

        with resources.path("cottonwood_data_mnist", data_filename) as path:
            with np.load(path) as data:
                self.images = data['training_images']
                self.labels = data['training_labels']

        self.n_images = self.images.shape[0]
        self.counter = -1
        self.indices = np.arange(len(self.labels), dtype=int)
        np.random.shuffle(self.indices)

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return("MNIST training data")

    def forward_pass(self, forward_in=None):
        self.counter += 1
        if self.counter == self.n_images:
            # Reshuffle the training images after each epoch -
            # each complete pass through the training set
            np.random.shuffle(self.indices)
            self.counter = 0

        i_image = self.indices[self.counter]
        image = np.reshape(self.images[i_image, :, :], (28, 28, 1))
        image = np.array(image, dtype=np.float)
        label = np.where(self.labels[i_image])[0][0]
        self.forward_out = (image, label)
        return self.forward_out

    def backward_pass(self, backward_in=None):
        return self.backward_out


class TestingData:
    def __init__(self, data_filename='mnist.npz'):
        with resources.path("cottonwood_data_mnist", data_filename) as path:
            with np.load(path) as data:
                self.images = data['test_images']
                self.labels = data['test_labels']

        self.n_images = self.images.shape[0]
        self.counter = -1
        self.indices = np.arange(len(self.labels), dtype=int)
        np.random.shuffle(self.indices)

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return("MNIST testing data")

    def forward_pass(self, forward_in=None):
        self.counter += 1
        if self.counter == self.n_images:
            # Reshuffle the training images after each epoch -
            # each complete pass through the training set
            np.random.shuffle(self.indices)
            self.counter = 0

        i_image = self.indices[self.counter]
        image = np.reshape(self.images[i_image, :, :], (28, 28, 1))
        image = np.array(image, dtype=np.float)
        label = np.where(self.labels[i_image])[0][0]
        self.forward_out = (image, label)
        return self.forward_out

    def backward_pass(self, backward_in=None):
        return self.backward_out


if __name__ == "__main__":
    print("from MNIST sample")
    training_data = TrainingData(data_filename="mnist_sample.npz")
    testing_data = TestingData(data_filename="mnist_sample.npz")
    for _ in range(10):
        image, label = training_data.forward_pass()
        print(f"training image shape: {image.shape}, label: {label}")
    for _ in range(10):
        image, label = testing_data.forward_pass()
        print(f"test image shape: {image.shape}, label: {label}")

    print()
    print("from MNIST entire")
    training_data = TrainingData()
    testing_data = TestingData()
    for _ in range(10):
        image, label = training_data.forward_pass()
        print(f"training image shape: {image.shape}, label: {label}")
    for _ in range(10):
        image, label = testing_data.forward_pass()
        print(f"test image shape: {image.shape}, label: {label}")
