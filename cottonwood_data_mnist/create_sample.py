import numpy as np

n_samples = 100
sample_npz = 'mnist_sample.npz'

with np.load('mnist.npz') as data:
    training_images_array = data['training_images']
    training_labels_array = data['training_labels']
    test_images_array = data['test_images']
    test_labels_array = data['test_labels']

training_images_sample = training_images_array[:n_samples, :, :]
training_labels_sample = training_labels_array[:n_samples, :]
test_images_sample = test_images_array[:n_samples, :, :]
test_labels_sample = test_labels_array[:n_samples, :]

np.savez_compressed(
    sample_npz,
    training_images=training_images_sample,
    training_labels=training_labels_sample,
    test_images=test_images_sample,
    test_labels=test_labels_sample,
)
